const mongoose = require('mongoose');
const validator = require('validator');
const autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);

const userSchema = new mongoose.Schema({
    city: {
        type: String,
        required: [true, 'Please enter your city']
    },
    start_date: {
        type: Date,
        required: [true, 'Please select start date']
    },
    price: {
        type: Number,
        required: [true, 'Please enter your price']
    },
    end_date: {
        type: Date,
        required: [true, 'Please select end date'],
        validate: {
            validator: function (endDate) {
                // Validate end date so that it must after start date 
                if (endDate <= this.start_date) { 
                    return false;
                } 
                return true;
            },
            message: 'End date must be after start date'
        }
    },
    status: {
        type: String,
        required: [true, 'Please enter status'],
    },
    color: {
        type: String,
        required: [true, 'Please select color'],
    }
});

userSchema.plugin(autoIncrement.plugin, { model: 'User' });

const User = mongoose.model('User', userSchema);

module.exports = User;