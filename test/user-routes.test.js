//During the test the env variable is set to test
process.env.NODE_ENV = "test";

let mongoose = require("mongoose");
const User = require("../models/userModel");

//Require the dev-dependencies
let chai = require("chai");
let chaiHttp = require("chai-http");
let server = require("../server");
let should = chai.should();
let recentAddedUserId;

chai.use(chaiHttp);
//Our parent block
describe("Started unit testing", () => {
  describe("CRUD USER TEST", () => {
    it("it should get all the users", async () => {
      let res = await chai.request(server).get("/api/v1/users");
      res.should.have.status(200);
      res.body.status.should.eq("success");
    });

    it("it should add user", async () => {
      let user = {
        city: "Indore",
        start_date: "4/3/2012",
        end_date: "4/3/2014",
        status: "Never",
        color: "#e6eeb9",
        price: 12.5,
      };

      let res = await chai
        .request(server)
        .post("/api/v1/users")
        .set("Connection", "keep alive")
        .set("Content-Type", "application/json")
        .type("form")
        .send(user);

      recentAddedUserId = res.body.data.doc._id;
      res.should.have.status(201);
      console.log(
        "============================================================"
      );
      console.log("Response Created User:", res.body);
    });

    it("it should update recent added user", async () => {
      let user = {
        city: "Pune",
        start_date: "4/3/2012",
        end_date: "4/3/2014",
        status: "Never",
        color: "#e6eeb9",
        price: 12.5,
      };

      let res = await chai
        .request(server)
        .patch("/api/v1/users/" + recentAddedUserId)
        .set("Connection", "keep alive")
        .set("Content-Type", "application/json")
        .type("form")
        .send(user);

      res.should.have.status(200);
      console.log(
        "============================================================"
      );
      console.log("Response Updated User:", res.body);
    });

    it("it should delete recently added users", async () => {
      let res = await chai
        .request(server)
        .delete("/api/v1/users/" + recentAddedUserId)
        .set("Connection", "keep alive")
        .set("Content-Type", "application/json")
        .type("form");
      res.should.have.status(204);
      console.log(
        "============================================================"
      );
    });
  });
});
