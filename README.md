# Node.js, Express and MongoDB Project Structure 
This is a basic project structure to help you to start building your own RESTful web APIs using Express framework and MongoDB with a good structure practices based on clean MVC Architecture.


# Features
- Fundamental of Express: routing, middleware, sending response and more
- Fundamental of Mongoose: Data models, data validation and middleware
- RESTful API including pagination and sorting
- CRUD operations with MongoDB
- Error handling
- Enviroment Varaibles
- handling error outside Express
- Catching Uncaught Exception

# Project Structure
- server.js : Responsible for connecting the MongoDB and starting the server.
- app.js : Configure everything that has to do with Express application. 
- config.env: for Enviroment Varaiables
- utils -> (Common logic) related to common functionality, Providing option for sorting, paggination, error handling etc. 
- routes -> userRoutes.js: The goal of the route is to guide the request to the correct handler function which will be in one of the controllers
- controllers -> userController.js: Handle the application request, interact with models and send back the response to the client 
- models -> userModel.js: (Business logic) related to business rules, how the business works and business needs ( Creating new user in the database, checking if the user password is correct, validating user input data)
- Test ->  ( Unit Testing ) This folders stands for test case files with respect to funcnality. For testing uses frameworks chai, chai-http, mocha.

# How to build and run project 

- Install dependency -> npm install
- Testing -> npm run test
- Start Server -> npm start

# Env Variables
- PORT -> Port on which app will run
- DATABASE -> Database url
- DATABASE_PASSWORD -> Database passowrd
