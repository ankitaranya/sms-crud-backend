const User = require("../models/userModel");
const base = require("./baseController");
const APIFeatures = require("../utils/apiFeatures");

exports.getAllUsers = async (req, res, next) => {
  try {
    let countResult = 0;
    let objFilter = {};

    if (
      (req.query.start_date != 'undefined' && req.query.start_date != "") && 
      (req.query.end_date != undefined && req.query.end_date != "")) {
      objFilter = {
        start_date: {
          $gte: new Date(new Date(req.query.start_date).setHours(0, 0, 0)),
        },
        end_date: {
          $lt: new Date(new Date(req.query.end_date).setHours(23, 59, 59)),
        },
      };
    }

    if (req.query.filter != undefined && req.query.filter != "") {
      objFilter["$or"] = [{
        city: { $regex: req.query.filter, $options: "i" }},
        {status: { $regex: req.query.filter, $options: "i" }},
        {"color" : { $regex: req.query.filter, $options: "i" }}
      ];
    }

    await User.countDocuments(objFilter, function (err, result) {
      countResult = result;
    });

    const users = new APIFeatures(User.find(objFilter), req.query)
      .sort()
      .paginate();

    const doc = await users.query;

    res.status(200).json({
      status: "success",
      total: countResult,
      results: doc.length,
      data: doc,
    });
  } catch (error) {
    next(error);
  }
};

exports.getUser = base.getOne(User);
exports.createUser = base.createOne(User);
exports.updateUser = base.updateOne(User);
exports.deleteUser = base.deleteOne(User);
